import os
import random
from six.moves import cPickle as pickle
from tensorflow.python.platform import gfile
import glob
import TensorflowUtils as utils

def Read_CT_dataset(data_dir, partitions=[], limit=-1, mode='ct'):
    pickle_filename = "CTdata.pickle"
    pickle_filepath = os.path.join(data_dir, pickle_filename)
    if os.path.exists(pickle_filepath):
        os.remove(pickle_filepath)

    if not os.path.exists(pickle_filepath):
        if mode is 'ct':
            result = create_ct_list(data_dir, partitions, limit)
        elif mode is 'three_well':
            result = create_three_well_image_lists(data_dir)
        print ("Pickling ...")
        with open(pickle_filepath, 'wb') as f:
            pickle.dump(result, f, pickle.HIGHEST_PROTOCOL)
    else:
        print ("Found pickle file!")

    with open(pickle_filepath, 'rb') as f:
        result = pickle.load(f)
        training_records = result['training']
        validation_records = result['validation']
        del result
    return training_records, validation_records

def create_ct_list(image_dir, partitions ,limit=-1):
    if not gfile.Exists(image_dir):
        print("Image directory '" + image_dir + "' not found.")
        return None
    directories = ['training', 'validation']
    # partitions = ['xyLD', 'xyLU']
    image_list = {}
    for directory in directories:
        image_list[directory] = []
    for patiation in partitions:
        for directory in directories:
            if not os.path.exists(os.path.join(image_dir, directory, patiation)):
                continue
            print("%s found in %s" % (patiation, directory))
            file_list = []

            # '*0000000*' for 0-9 files
            # '*000000*' for 0-99 files
            # '*00000*' for 0-999 files
            # file_glob = os.path.join(image_dir, directory, patiation, 'ori', '*0000000*')
            file_glob = os.path.join(image_dir, directory, patiation, 'ori', '*')
            file_list.extend(glob.glob(file_glob))

            if not file_list:
                print('No files found')
            else:
                for f in file_list:
                    filename = os.path.splitext(f.split(os.sep)[-1])[0]
                    annotation_file = os.path.join(image_dir, directory, patiation, 'bin', filename.replace('ori', 'bin'))
                    if os.path.exists(annotation_file):
                        record = {'image': f, 'annotation': annotation_file, 'filename': filename}
                        image_list[directory].append(record)
                    else:
                        print("Annotation file not found for %s - Skipping" % filename)
            random.shuffle(image_list[directory])
    for directory in directories:
        no_of_images = len(image_list[directory])
        if limit != -1 and limit > 1 and limit < no_of_images:
            image_list[directory] = image_list[directory][:limit]
        no_of_images = len(image_list[directory])
        print('No. of %s files: %d' % (directory, no_of_images))
        # {'image': 'F:\\CTdata\\images\\training\\xyLDori_subVol_00000007', 'filename': 'xyLDori_subVol_00000007','annotation': 'F:\\CTdata\\annotations\\training\\xyLDbin_subVol_00000007'}

    return image_list


def create_three_well_image_lists(image_dir, file_type='.npy'):
    if not gfile.Exists(image_dir):
        print("Image directory '" + image_dir + "' not found.")
        return None
    directories = ['training', 'validation']
    image_list = {}

    for directory in directories:
        file_list = []
        image_list[directory] = []
        file_glob = os.path.join(image_dir, "images", directory, '*' + file_type)
        file_list.extend(glob.glob(file_glob))

        if not file_list:
            print('No files found')
        else:
            for f in file_list:
                filename = os.path.splitext(f.split(os.path.sep)[-1])[0]
                annotation_file = os.path.join(image_dir, "annotations", directory, filename.replace('_image', '_anno') + file_type)
                if os.path.exists(annotation_file):
                    record = {'image': f, 'annotation': annotation_file, 'filename': filename}
                    image_list[directory].append(record)
                else:
                    # print("Annotation file not found for %s - Skipping" % filename)
                    print(annotation_file)

        random.shuffle(image_list[directory])
        no_of_images = len(image_list[directory])
        print ('No. of %s files: %d' % (directory, no_of_images))

    return image_list


if __name__ == '__main__':
    image_dir = 'H:\\Three_Well_data_v2'
    # print(create_three_well_image_lists(image_dir))
    # print(Read_CT_dataset(image_dir,mode='three_well'))