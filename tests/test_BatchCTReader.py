

import json
import unittest
import configparser
import read_CTParsingData as ctr
import tensorflow as tf
from BatchDatset_IGW import BatchDatset_IGW as dataset

class Test_BatchCTReader(unittest.TestCase):
    def setUp(self):
        conf = configparser.ConfigParser()
        conf.read("../para.conf")
        self.FLAGS = tf.flags.FLAGS

        conf_dict = {}
        conf_dict['batch_size'] = conf.get("parameter", "batch_size")
        conf_dict['logs_dir'] = conf.get("path_setting", "logs_dir")
        conf_dict['data_dir'] = conf.get("path_setting", "data_dir")
        conf_dict['learning_rate'] = conf.get("parameter", "learning_rate")
        conf_dict['model_dir'] = conf.get("path_setting", "model_dir")
        conf_dict['debug'] = conf.get("execution_mode", "debug")
        conf_dict['mode'] = conf.get("execution_mode", "mode")
        conf_dict['MAX_ITERATION'] = conf.get("parameter", "MAX_ITERATION")
        conf_dict['Train_Image_Limit'] = conf.get("parameter", "Train_Image_Limit")
        conf_dict['partitions'] = conf.get("path_setting", "partitions")
        conf_dict['task'] = conf.get("execution_mode", "task")
        conf_dict['image_size'] = conf.get("parameter", "image_size")
        conf_dict['num_of_classess'] = conf.get("parameter", "num_of_classess")

        tf.flags.DEFINE_integer("batch_size", conf_dict['batch_size'], "batch size for training")
        tf.flags.DEFINE_string("logs_dir", conf_dict['logs_dir'], "path to logs directory")
        tf.flags.DEFINE_string("data_dir", conf_dict['data_dir'], "path to dataset")
        # tf.flags.DEFINE_string("model_checkpoint_path", conf_dict['model_checkpoint_path'], "path to dataset")
        tf.flags.DEFINE_float("learning_rate", conf_dict['learning_rate'], "Learning rate for Adam Optimizer")
        tf.flags.DEFINE_string("model_dir", conf_dict['model_dir'], "Path to vgg model mat")
        tf.flags.DEFINE_bool('debug', conf_dict['debug'], "Debug mode: True/ False")
        tf.flags.DEFINE_string('mode', conf_dict['mode'], "Mode train/ test/ visualize")
        MAX_ITERATION = int(conf_dict['MAX_ITERATION'])
        limit = int(conf_dict['Train_Image_Limit'])
        partitions = conf_dict['partitions'].split(',')
        tf.flags.DEFINE_string('task', conf_dict['task'], "Task ct/ three_well")

        NUM_OF_CLASSESS = int(conf_dict['num_of_classess'])
        IMAGE_SIZE = int(conf_dict['image_size'])

        self.image_options = {'resize': True, 'resize_size': IMAGE_SIZE}



    def test_random_reader(self):

        train_records, valid_records = ctr.Read_CT_dataset(self.FLAGS.data_dir, mode='three_well')
        validation_dataset_reader = dataset(valid_records, self.image_options)
        # valid_images, valid_annotations, indexes = validation_dataset_reader.get_random_batch(self.FLAGS.batch_size)
        valid_images, valid_annotations, indexes = validation_dataset_reader.get_sequence_batch_size(self.FLAGS.batch_size)

        num_set = set(indexes)
        num = []
        for _num in num_set:
            num.append(_num)

        print(indexes)

        self.assertEquals(len(num), self.FLAGS.batch_size)



