"""
Code ideas from https://github.com/Newmu/dcgan and tensorflow mnist dataset reader
"""
import numpy as np
import scipy.misc as misc
import os
# from PIL import Image


class BatchDatset:
    files = []
    images = []
    annotations = []
    image_options = {}
    batch_offset = 0
    epochs_completed = 0

    def __init__(self, records_list, image_options={}):
        print("Initializing Batch Dataset Reader...")
        print(image_options)
        self.files = records_list
        self.image_options = image_options
        # self._read_images()
        self._read_images_ctr()

    # original mode
    # def _read_images(self):
    #     self.__channels = True
    #     # self.images = np.array([self._transform(filename['image']) for filename in self.files])
    #     self.images = np.array([self._transform(filename['image']) for filename in self.files])
    #     self.__channels = False
    #     self.annotations = np.array(
    #         [np.expand_dims(self._transform(filename['annotation'], type='annotation'), axis=3) for filename in self.files])
    #     # self.annotations = np.array(
    #     #     [self._transform(filename['annotation']) for filename in self.files])
    #     print (self.images.shape)
    #     print (self.annotations.shape)


    # def _transform(self, filename, type='image'):
    #     # image = misc.imread(filename)
    #     image= self._read_ct_file(filename,type)
    #     # if self.__channels and len(image.shape) < 3:  # make sure images are of shape(h,w,3)
    #     #     image = np.array([image for i in range(3)])
    #
    #     if self.image_options.get("resize", False) and self.image_options["resize"] and type=='image':
    #         resize_size = int(self.image_options["resize_size"])
    #         # resize_image = misc.imresize(image,
    #         #                              [resize_size, resize_size], interp='nearest')
    #         resize_image = misc.imresize(image, [resize_size, resize_size], interp='nearest', mode='P')
    #     elif type=='annotation':
    #         resize_size = int(self.image_options["resize_size"])
    #         resize_image = misc.imresize(image, [resize_size, resize_size], interp='nearest', mode='L')
    #     else:
    #         resize_image = image
    #     return np.array(resize_image)

    def _read_images_ctr(self):
        self.images = np.array([self._centralize(filename['image']) for filename in self.files])
        self.annotations = np.array([self._centralize(filename['annotation']) for filename in self.files])
        print (self.images.shape)
        print (self.annotations.shape)

    def _centralize(self, filename, size=200):
        image = self._read_ct_file(filename)
        resize_image_x = np.hsplit(image, 4)[0]
        resize_image = np.vsplit(resize_image_x, 4)[0]
        resize_image = resize_image.reshape(size, size, 1)
        return resize_image

    def get_records(self):
        return self.images, self.annotations

    def reset_batch_offset(self, offset=0):
        self.batch_offset = offset

    def next_batch(self, batch_size):
        start = self.batch_offset
        self.batch_offset += batch_size
        if self.batch_offset > self.images.shape[0]:
            # Finished epoch
            self.epochs_completed += 1
            print("****************** Epochs completed: " + str(self.epochs_completed) + "******************")
            # Shuffle the data
            perm = np.arange(self.images.shape[0])
            np.random.shuffle(perm)
            self.images = self.images[perm]
            self.annotations = self.annotations[perm]
            # Start next epoch
            start = 0
            self.batch_offset = batch_size

        end = self.batch_offset
        return self.images[start:end], self.annotations[start:end]

    def get_random_batch(self, batch_size):
        indexes = np.random.randint(0, self.images.shape[0], size=[batch_size]).tolist()
        return self.images[indexes], self.annotations[indexes], indexes

    def get_sequence_batch_size(self, batch_size):
        if len(self.images) < batch_size:
            AssertionError('images size is less than batch size')
        indexes = [i for i in range(batch_size)]
        return self.images[indexes], self.annotations[indexes], indexes

    def _read_ct_file(self, filepath, IMAGE_SIZE=800, type='image'):
        array = []
        if not os.path.exists(filepath):
            print(filepath + ' not exist')
            return 0
        with open(filepath, "r") as ins:
            flag=0
            for line in ins:
                if line == '\n':
                    continue
                if flag == 1:
                    array.append(line.strip())
                if line == '@1\n':
                    flag=1
        data = np.array([int(x) for x in array], np.uint8).reshape(IMAGE_SIZE, IMAGE_SIZE)
        return data
