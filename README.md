# CTDataSegmentation_split

Tensorflow implementation of [Fully Convolutional Networks for Semantic Segmentation](http://arxiv.org/pdf/1605.06211v1.pdf) (FCNs). 

Author: benchuang

Original Author: shekkizh [https://github.com/shekkizh/FCN.tensorflow](https://github.com/shekkizh/FCN.tensorflow)

1. [Roadmap](#Roadmap)
1. [Prerequisites](#Prerequisites)
2. [How to Execution](#How to Execution)
* 1. [Main Program](###Main program)
* * *  [Data directory structure](####Data directory structure)
* * * [Config](####Config )
* * * [Execution](####Execution)
* 2. [Tool](##Tool)
* * * [Image_Transfer_File.py](###Image_Transfer_File.py)
* * * [Viewer CT_image.ipynb](###[Viewer]CT_image.ipynb)
1. [QA handling](###QA handling)


## Roadmap
1. 之後的應用模式可為-新照片先做驗證再做訓練
2. ~~Image_Transfer_File.py的output標示像素大小~~(done)
3. ~~visulize mode的image標示預測的原圖名稱~~(done)
4. ~~把會儲存image路徑的CTdata.pickle的移除~~(done)
5. ~~train的時間標註，validation時間，標在log檔案中~~(done)
6. ~~補充requirements python執行環境可以使用GPU~~(done)
7. 萃取deconvoluation過程的數值(2月後)
8. 漂流樹木圖像的FCN(2月後)
9. 3、4月將原始圖改為200*200，計算相同為200*200
10. 網路資源樹木image與label

## Reuslt
![result](image/prediction_result.png)


## Prerequisites
* anaconda3
* python==3.5.2
* tensorflow==1.3.0


Environment installation steps
```
#conda create -n [環境名稱] python=3.5.2
#source activate [環境名稱] 
#conda install -c anaconda tensorflow-gpu-base==1.3.0
#pip install -r requirements.txt
```

## How to Execution

### Main program

#### Data directory structure

```
CTdata/
├── training
│   └── xyLD
│       ├── bin
│       └── ori
└── validation
    └── xyLU
        ├── bin
        └── ori
```

#### Config 

config file: para.conf


##### data location [path_setting]
`
data_dir = F:\CTdata
`

##### The name of data directories [path_setting]
`
partitions = xyLD,xyLU
`

the all directories in training and validation, and split with comma

##### Mode train/ visualize [execution_mode]
`
mode = visualize
`

Config mode to train or visualize. 

Train mode will pick randomly in the image in training directories to train in iterations.

Visualize mode will pick randomly in the image in validation directories, and output the image files and sequence pixels files. 

##### batch size for training [parameter]
`
batch_size = 2
`

How many image to be train or visualize in one iteration.

##### Learning rate for Adam Optimizer [parameter]
`
learning_rate = 1e-4
`

In training mode, setting the parameter of Adam Optimizer. 

##### Train iteration [parameter]
`
MAX_ITERATION = 100001
`

In training mode, setting the number of your training iteration.  

##### Limit No of images to train  [parameter]
`
Train_Image_Limit = 200
`

Limit the number of both training and validation images.

If you have 2000 images in training directories and your limit value is 200, the program will pick 200 images randomly in 2000 images.

(No limit option is -1.)


#### Execution
Execution command:
```
python test.py
```

## Tool

#### Image_Transfer_File.py

Goal: Transfer CT image to sequence pixels file. 

Execution command:
```
python Image_Transfer_File.py [image_path]
```
Output file:
```
Locate in the directory of the image. 
```

#### [Viewer]CT_image.ipynb

Program location: utils/

Goal: The viewer examines the original, label and prediction image.

 - Running with Jupyter
 - Assign path with the image directory
```
log_path = r'C:\Users\today459\PycharmProjects\CTDataSegmentation_split\logs\ctdata'
```



# QA handling

Q1.

When ruuning training and testing tensorflow gpu error log?

2018-03-13 22:07:14.785250: E tensorflow/stream_executor/cuda/cuda_driver.cc:406] failed call to cuInit: CUDA_ERROR_NOT_INITIALIZED

Ans.
Reboot server and the server will detect device. 