
import numpy as np
import scipy.misc as misc
import os
from BatchCTReader import BatchDatset
from read_CTParsingData import Read_CT_dataset

class BatchDatset_IGW(BatchDatset):

    def _centralize(self, filename, size=101):
        image = self._read_ct_file(filename, 'npy')
        if np.ndim(image) == 2:
            image = image.reshape(size, size, 1)
        return image

    def _read_images_ctr(self):
        self.images = np.array([self._centralize(filename['image'], size=21) for filename in self.files])
        self.annotations = np.array([self._centralize(filename['annotation'], size=20) for filename in self.files])
        print (self.images.shape)
        print (self.annotations.shape)

    def _read_ct_file(self, filepath, type='image'):
        if type is 'image':
            image = misc.imread(filepath, mode='P')
        elif type is 'npy':
            image = np.load(filepath)
        return image

if __name__ == '__main__':
    image_dir = 'H:\\Three_Well_data_v2'
    train_records, valid_records = Read_CT_dataset(image_dir,mode='three_well')
    BatchDatset_IGW(valid_records)
