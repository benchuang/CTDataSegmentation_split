subroutine tecplot_fig_python(interface_fig_inf,interface_outfile_inf) bind(c, name="tec_fig_2D")
use, intrinsic :: ISO_C_BINDING
implicit none

type, BIND(C) :: interface_fig
   integer (C_INT) :: no_x,no_y
   type (C_PTR) fig
end type interface_fig

type, BIND(C) :: interface_outfile
   integer (C_INT) :: file_ind
end type interface_outfile


type (interface_fig), intent(in) :: interface_fig_inf
type (interface_outfile), intent(in) :: interface_outfile_inf

!!!!!!!!!!!!!!!!!! pointer interface_fig_inf
real (C_DOUBLE), pointer:: fig(:)
!!!!!!!!!!!!!!!!!! pointer interface_fig_inf

!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf
call C_F_POINTER(interface_fig_inf%fig, fig, [interface_fig_inf%no_x*interface_fig_inf%no_y])
!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf

call tecplot_fig(&
!interface_fig_inf
& interface_fig_inf%no_x,&
& interface_fig_inf%no_y,&
& fig,&
!interface_outfile_inf
& interface_outfile_inf%file_ind)


end subroutine

    
