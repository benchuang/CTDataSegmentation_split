subroutine tecplot_fig(&
!interface_fig_inf
& no_x,&
& no_y,&
& fig,&
!interface_outfile_inf
& file_ind)

integer*4 ind
integer*4 i,j,k
integer*4 no_x,no_y
real*8 fig(no_x*no_y)
integer*4 file_ind
character(256) filename

write(filename,"(A3,I7,A4)") 'RES',file_ind,'.dat'

ind=333

open(ind,file=filename)
write(ind,"(A27)") 'Title=" 3-D Model Results "'
write(ind,"(A34)") 'VARIABLES="x(m)","y(m)","K(m/day)"'
write(ind,*) 'ZONE I=',no_x,', J=',no_y,', F=Point'
do j=1,no_y
   do i=1,no_x
      write(ind,*) i-1,j-1,fig((j-1)*no_x+i)
   end do
end do

close(333)
end 
