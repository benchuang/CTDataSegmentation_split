import scipy.misc as misc
import argparse
import os
import numpy as np

def main(image_path):
    if os.path.exists(image_path):
        try:
            img = misc.imread(image_path)
            image_shape = np.array(img).shape
            arr = np.array(img).flatten()
            with open(image_path.split(os.pathsep)[-1].split('.')[0], 'w') as f:
                f.write('%sx%s\n' % (image_shape))
                for i in arr:
                    f.write(str(i)+'\n')
                f.close()
            print('%s array file output' % (image_path.split(os.pathsep)[-1].split('.')[0]))
        except Exception as e:
            print(e)
    else:
        print('%s image not found.' % (image_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('image_path', help='image path, .png')
    args = parser.parse_args()
    main(args.image_path)


# usage: Image_Transfer_File.py [-h] image_path
#
# positional arguments:
#   image_path  image path, .png

# example
# python Image_Transfer_File.py C:\Users\today459\Desktop\iter_100000\pred_6.png
# output:
    # C:\Users\today459\Desktop\iter_100000\pred_6